'use strict'

function getRanked(ranked, scores){
    var result = [];
    ranked = [...new Set(ranked)];
    var i = 0;
    for (const score of scores) {
        while (i < ranked.length && ranked[ranked.length-i-1] <= score) {
            i = i + 1;
        }
        result.push(ranked.length-i+1);
    }
    return result
}
module.exports = {
    getRanked
  }