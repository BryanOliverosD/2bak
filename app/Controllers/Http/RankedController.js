'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Ranked = use ('App/Models/Ranked');
/**
 * Resourceful controller for interacting with rankeds
 */
class RankedController {
  /**
   * Show a list of all rankeds.
   * GET rankeds
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    try{
      const ranked_list = await Ranked.all();
      if (ranked_list['rows'] != 0){
        return response.status(200).send({status: 'success', ranking: ranked_list});
      } else {
        return response.status(400).send({status: 'SIN ELEMENTOS', ranking: ranked_list});
      }
    } catch (error){
      return response.status(500).send({error:'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to be used for creating a new ranked.
   * GET rankeds/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create (ranking, score_ids) {
    try{
      var i = 0
      for (const score of score_ids) {
  
        const new_ranked = new Ranked();
        new_ranked.score_id = score;
        new_ranked.ranking = ranking[i];
        await new_ranked.save();
        i = i + 1;
      }
      return true
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Create/save a new ranked.
   * POST rankeds
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single ranked.
   * GET rankeds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ request, response}) {
    try {
      const params = request.get();
      const ranked = await Ranked.findBy('id',params.id);
      if (!ranked) {
          return response.status(404).json({data: 'Ranking not found'});
      }
     return response.status(200).json({status:true, data: ranked});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to update an existing ranked.
   * GET rankeds/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update ranked details.
   * PUT or PATCH rankeds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ request, response }) {
    try {
      const params = request.get();
      const ranked = await Ranked.findBy('id',params.id);
      if (!ranked) {
          return response.status(404).json({data: 'Ranking not found'});
      }
      ranked.merge(request.post());
      await ranked.save()
      return response.status(200).json({status:true, data: ranked});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Delete a ranked with id.
   * DELETE rankeds/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ request, response }) {
    try {
      const params = request.get();
      const ranked = await Ranked.findBy('id',params.id);
      if (!ranked) {
          return response.status(404).json({data: 'ranking not found'});
      }
      await ranked.delete();
      return response.status(200).json({status:true, data: ranked});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }
}

module.exports = RankedController
