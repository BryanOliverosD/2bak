'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Player = use ('App/Models/Player');
const ScoreController = use ('App/Controllers/Http/ScoreController');
const RankedController = use ('App/Controllers/Http/RankedController');
const {getRanked} = use('App/Helpers')
/**
 * Resourceful controller for interacting with players
 */
class PlayerController {
  /**
   * Show a list of all players.
   * GET players
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response}) {
    try{
      const player_list = await Player.all();
      if (player_list['rows'] != 0){
        return response.status(200).send({status: 'success', players: player_list});
      } else {
        return response.status(400).send({status: 'SIN ELEMENTOS', players: player_list});
      }
    } catch (error){
      return response.status(500).send({error:'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to be used for creating a new player.
   * GET players/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response }) {
    try{
      const player = await Player.findBy('email', request.input('email'));
      const ranked = request.input('ranked');
      const score_controller = new ScoreController();
      const ranked_controller = new RankedController();
      if (player){
        const score = await score_controller.create(player.id, request.input('score'));
        player.max_score = (score[1] > player.max_score) ? score[1] : player.max_score;
        await player.save();
        const result_ranking = getRanked(ranked, score[0]);
        await ranked_controller.create(result_ranking, score[2]);
        return response.status(200).send({status:'success', puntuacion: result_ranking});
      } else {
        const new_player = new Player();
        new_player.email = request.input('email');
        new_player.max_score = Math.max(...request.input('score'));
        await new_player.save();
        const score = await score_controller.create(new_player.id, request.input('score'));
        const result = getRanked(ranked, score[0]);
        //return response.status(200).send({status:'success', player: new_player, score: score[0]});
        return response.status(200).send({status:'success', puntuacion: result});
      }
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Create/save a new player.
   * POST players
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single player.
   * GET players/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ request, response}) {
    try {
      const params = request.get();
      const player = await Player.findBy('email',params.email);
      if (!player) {
          return response.status(404).json({data: 'Player not found'});
      }
     return response.status(200).json({status:true, data: player});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to update an existing player.
   * GET players/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({request, response}) {
  }

  /**
   * Update player details.
   * PUT or PATCH players/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ request, response }) {
    try {
      const params = request.get();
      const player = await Player.findBy('email',params.email);
      if (!player) {
          return response.status(404).json({data: 'Player not found'});
      }
      player.merge(request.post());
      await player.save()
      return response.status(200).json({status:true, data: player});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Delete a player with id.
   * DELETE players/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ request, response }) {
    try {
      const params = request.get();
      const player = await Player.findBy('email',params.email);
      if (!player) {
          return response.status(404).json({data: 'Player not found'});
      }
      await player.delete();
      return response.status(200).json({status:true, data: player});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }
}

module.exports = PlayerController
