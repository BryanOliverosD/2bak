'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Score = use ('App/Models/Score');
const Database = use('Database');
/**
 * Resourceful controller for interacting with scores
 */
class ScoreController {
  /**
   * Show a list of all scores.
   * GET scores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    try{
      const score_list = await Score.all();
      if (score_list['rows'] != 0){
        return response.status(200).send({status: 'success', scores: score_list});
      } else {
        return response.status(400).send({status: 'SIN ELEMENTOS', scores: score_list});
      }
    } catch (error){
      return response.status(500).send({error:'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to be used for creating a new score.
   * GET scores/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create (player_id, scores) {
    var max_score = 0;
    var score_ids = [];
    try{
      for (const score of scores) {
        max_score = (score > max_score) ? score : max_score;    
        const new_score = new Score();
        new_score.player_id = player_id;
        new_score.score = score;
        await new_score.save();
        score_ids.push(new_score.id);
      }
      return [scores, max_score, score_ids]
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Create/save a new score.
   * POST scores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single score.
   * GET scores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ request, response }) {
    try {
      const params = request.get();
      const score = await Score.findBy('id',params.id);
      if (!score) {
          return response.status(404).json({data: 'Score not found'});
      }
     return response.status(200).json({status:true, data: score});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Render a form to update an existing score.
   * GET scores/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update score details.
   * PUT or PATCH scores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ request, response }) {
    try {
      const params = request.get();
      const score = await Score.findBy('id',params.id);
      if (!score) {
          return response.status(404).json({data: 'Score not found'});
      }
      score.merge(request.post());
      await score.save()
      return response.status(200).json({status:true, data: score});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }

  /**
   * Delete a score with id.
   * DELETE scores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ request, response }) {
    try {
      const params = request.get();
      const score = await Score.findBy('id',params.id);
      if (!score) {
          return response.status(404).json({data: 'Score not found'});
      }
     await score.delete();
     return response.status(200).json({status:true, data: score});
    } catch (error){
      console.log(error);
      return response.status(500).send({error: 'ERROR INTERNO'});
    }
  }
 /* async deleteFromPlayer(player_id) {
    try {
      const scores = await Database.table('scores').where('player_id',player_id).delete();
      return [200, scores]
    } catch (error){
      console.log(error);
      return [500, error]
    }
  }*/
}

module.exports = ScoreController
