'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RankedSchema extends Schema {
  up () {
    this.create('rankeds', (table) => {
      table.increments()
      table.integer('score_id').unsigned().references('id').inTable('scores')
      table.integer('ranking').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('rankeds')
  }
}

module.exports = RankedSchema
