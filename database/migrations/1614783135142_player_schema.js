'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlayerSchema extends Schema {
  up () {
    this.create('players', (table) => {
      table.increments()
      table.string('email', 254).notNullable().unique()
      table.integer('max_score').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('players')
  }
}

module.exports = PlayerSchema
