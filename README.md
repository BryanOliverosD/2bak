# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

### ENDPOINTS

http://0.0.0.0:3333/api/ranked => POST Encargado de entregar la clasificación del jugador. IN:ranked[], score[] / OUT:puntuacion[]

http://0.0.0.0:3333/api/player => DELETE Elimina un player de la base de datos. IN:email:string / OUT:Player Eliminado{}
http://0.0.0.0:3333/api/player => GET Obtiene información de un Player a través de su email. IN:email:string / OUT:Player{}
http://0.0.0.0:3333/api/player => PUT Actualiza registro con los datos enviados. IN:email:string y/o max_score:int / OUT:Player Actualizado{}

http://0.0.0.0:3333/api/score => DELETE Elimina un score de la base de datos. IN:id_score:int / OUT:Score Eliminado{}
http://0.0.0.0:3333/api/score => GET Obtiene información de un Score a través de su id. IN:id:int / OUT:Score{}
http://0.0.0.0:3333/api/score => PUT Actualiza registro con los datos enviados. IN:id:id y/o player_id:int y/o score:int / OUT:Score Actualizado{}


