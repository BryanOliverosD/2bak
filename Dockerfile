#Nombre de la imagen a utilizar (debe ir incluido el tag).
FROM node:14

#Definir el espacio de trabajo (debe ser el mismo que se creo arriba)
WORKDIR /app

COPY .env.example .env
#copiar todos los archivos del proyecto con execpción de los ingresados en el .dockerignore
COPY . /app

#Ejecutar comando npm install
RUN npm install
RUN npm i -g @adonisjs/cli nodemon

#Definir puerto de salida si es necesario.
EXPOSE 3333


CMD ["npm", "start"]