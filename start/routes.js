'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('/api/ranked', 'PlayerController.create')

Route.get('/api/player', 'PlayerController.index')
Route.delete('/api/player', 'PlayerController.destroy')
Route.get('/api/player/:email', 'PlayerController.show')
Route.put('/api/player', 'PlayerController.update')

Route.get('/api/score', 'ScoreController.index')
Route.delete('/api/score', 'ScoreController.destroy')
Route.get('/api/score/:id', 'ScoreController.show')
Route.put('/api/score', 'ScoreController.update')

Route.get('/api/ranking', 'RankedController.index')
Route.delete('/api/ranking', 'RankedController.destroy')
Route.get('/api/ranking/:id', 'RankedController.show')
Route.put('/api/ranking', 'RankedController.update')